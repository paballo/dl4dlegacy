
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
        <meta name="description" content="" />
        <meta name="author" content="" />
        <title> {{ config('app.name', 'Laravel') }}</title>
        <link href="https://cdn.jsdelivr.net/npm/simple-datatables@latest/dist/style.css" rel="stylesheet" />
        <link href="https://cdn.jsdelivr.net/npm/litepicker/dist/css/litepicker.css" rel="stylesheet" />
        <link href="https://sb-admin-pro.startbootstrap.com/css/styles.css" rel="stylesheet" />
        <link rel="icon" type="image/x-icon" href="assets/img/favicon.png" />
        <script data-search-pseudo-elements="" defer="" src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.1.1/js/all.min.js" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/feather-icons/4.28.0/feather.min.js" crossorigin="anonymous"></script>
        <link href="{{ asset('/assets/css/main.css') }}" rel="stylesheet">
    </head>
    <body class="nav-fixed">
        <nav class="topnav navbar navbar-expand shadow justify-content-between justify-content-sm-start navbar-light bg-white" id="sidenavAccordion">
            <!-- Sidenav Toggle Button-->
            <button class="btn btn-icon btn-transparent-dark order-1 order-lg-0 me-2 ms-lg-2 me-lg-0" id="sidebarToggle"><i data-feather="menu"></i></button>
            <!-- Navbar Brand-->
            <!-- * * Tip * * You can use text or an image for your navbar brand.-->
            <!-- * * * * * * When using an image, we recommend the SVG format.-->
            <!-- * * * * * * Dimensions: Maximum height: 32px, maximum width: 240px-->
            <a class="navbar-brand pe-3 ps-4 ps-lg-2" href="{{ url('/') }}"> {{ config('app.name', 'Laravel') }}</a>
            <!-- Navbar Search Input-->
            <!-- * * Note: * * Visible only on and above the lg breakpoint-->
           
        </nav>
        <div id="layoutSidenav">
            <div id="layoutSidenav_nav">
                <nav class="sidenav shadow-right sidenav-light">
                    <!-- Sidenav Accordion (Dashboard)-->
                    <a class="nav-link collapsed p-4" href="/" >
                        Overview
                    </a>
                    <a class="nav-link collapsed p-4" href="/Province/Gauteng">
                        Gauteng 
                    </a>
                    <a class="nav-link collapsed p-4" href="/province/Free state">
                        Free state
                       
                    </a>
                    <a class="nav-link collapsed p-4" href="/province/Eastern Cape">
                        Eastern Cape
                        
                    </a>
                    <a class="nav-link collapsed p-4" href="/province/Limpopo">
                        Limpopo
                        
                    </a>
                    <a class="nav-link collapsed p-4" href="/province/kzn">
                        Kwa-Zulu-Natal
                       
                    </a>
                    <a class="nav-link collapsed p-4" href="/province/Mpumalanga">
                        Mpumalanga
                       
                    </a>
                    <a class="nav-link collapsed p-4" href="/province/North West">
                        North West
                       
                    </a>
                    <a class="nav-link collapsed p-4" href="/province/Northern Cape">
                        Northern Cape

                    </a>

                    <a class="nav-link collapsed p-4" href="/province/Western Cape">
                        Western Cape

                    </a>
                          
                </nav>
            </div>
            <div id="layoutSidenav_content">
           
                @yield('content')
                
            </div>
        </div>
        <script data-cfasync="false" src="/cdn-cgi/scripts/5c5dd728/cloudflare-static/email-decode.min.js"></script><script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" crossorigin="anonymous"></script>
        <script src="https://sb-admin-pro.startbootstrap.com/js/scripts.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.9.4/Chart.min.js" crossorigin="anonymous"></script>
        <script src="https://sb-admin-pro.startbootstrap.com/assets/demo/chart-area-demo.js"></script>
        <script src="https://sb-admin-pro.startbootstrap.com/assets/demo/chart-bar-demo.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/simple-datatables@latest" crossorigin="anonymous"></script>
        <script src="js/datatables/datatables-simple-demo.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/litepicker/dist/bundle.js" crossorigin="anonymous"></script>
        <script src="https://sb-admin-pro.startbootstrap.com/js/litepicker.js"></script>

       
</body>
</html>
