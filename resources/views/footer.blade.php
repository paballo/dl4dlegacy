<div class="demo-row">
    <div class="container" id="id-sponsors">
      <div class="text-center">
      <h2 style="margin:20px 0;color:black;">Partners</h2>
    </div>
    <div id="sponsor-carousel" class="carousel slide" data-ride="carousel"> 
      
      <!-- Wrapper for slides -->
      <div class="carousel-inner" role="listbox">
        <div class="item active">
          <div class="row">
            <div class="col-sm-4 col-xs-6">
              <div class="sponsor-feature"><img alt="" src="https://www.studentroom.co.za/wp-content/uploads/2021/08/cpsi-logo1-1200x720.jpg" style="width: 200px;" /></div>
            </div>
            <div class="col-sm-4 col-xs-6">
              <div class="sponsor-feature"><img alt="" src="https://www.opendata.durban/odd/static/assets/img/projects/project-logo_opendataza.svg" style="width: 155px;" /></div>
            </div>
            <div class="col-sm-4 col-xs-6">
              <div class="sponsor-feature"><img alt="" src="https://nimd.org/wp-content/uploads/2018/06/british-high-cmsn-clr-logo-2.png" style="width: 200px; height:90px" /></div>
            </div>
          </div>
        </div>
      </div>
    </div>
    </div>
    </div>