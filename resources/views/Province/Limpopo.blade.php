@extends('welcome')
<link href="{{ asset('/assets/css/main.css') }}" rel="stylesheet">
<link href="{{ asset('/assets/css/style.css') }}" rel="stylesheet">
@section('content')

<main>
        <body class="is-preload">
    
            <!-- Wrapper -->
                <div id="wrapper">
    
                    <!-- Main -->
                        <div id="main">
    
                            <!-- Post -->
                                <article class="post">
                                    <header>
                                        <div class="title">
                                            <h2>Limpopo</h2>
                                            <p>Lorem ipsum dolor amet nullam consequat etiam feugiat</p>
                                        </div>
                                        <div class="meta">
                                            <time class="published" datetime="2015-11-01">November 1, 2015</time>
                                            <a href="#" class="author"><span class="name">Jane Doe</span><img src="images/avatar.jpg" alt="" /></a>
                                        </div>
                                    </header>
                                    <a href="" class="image featured"><img src="" alt="" /></a>
                                    <p>Mauris neque quam, fermentum ut nisl vitae, convallis maximus nisl. Sed mattis nunc id lorem euismod placerat. Vivamus porttitor magna enim, ac accumsan tortor cursus at. Phasellus sed ultricies mi non congue ullam corper. Praesent tincidunt sed tellus ut rutrum. Sed vitae justo condimentum, porta lectus vitae, ultricies congue gravida diam non fringilla.</p>
                                    <br>
                                    <div class="container" id="id-sponsors">
                                        <div class="text-center">
                                        <h2 style="margin:20px 0;color:black;">Limpopo Stats</h2>
                                      </div>
                                    <div class="wrap-circles">
                                        <div class="circle per-0">
                                          <div class="inner"><span style="color: black">0%</span></div>
                                        </div>
                                        <div class="circle per-25">
                                          <div class="inner"><span style="color: black">25%</span></div>
                                        </div>
                                        <div class="circle per-50">
                                          <div class="inner"><span style="color: black">50%</span></div>
                                        </div>
                                        <div class="circle per-75">
                                          <div class="inner"><span style="color: black">75%</span></div>
                                        </div>
                                        <div class="circle per-100">
                                          <div class="inner"><span style="color: black">100%</span></div>
                                        </div>
                                      </div>
                                      @include('/footer')
                                </article>
                        </div>
                </div>

        </body>  
</main>

@endsection