<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ProvinceController extends Controller
{
    public function gauteng(){
        return view("/province/Gauteng");
    }

    public function easterncape(){
        return view("/province/Eastern Cape");
    }

    public function kzn(){
        return view("/province/kzn");
    }

    public function limpopo(){
        return view("/province/Limpopo");
    }

    public function mpumalanga(){
        return view("/province/Mpumalanga");
    }

    public function northerncape(){
        return view("/province/Northern Cape");
    }

    public function northwest(){
        return view("/province/North West");
    }

    public function freestate(){
        return view("/province/Free state");
    }

    public function westerncape(){
        return view("/province/Western Cape");
    }

}
