<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ProvinceController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/Province/Gauteng', [ProvinceController::class, 'gauteng']);
Route::get('/province/Eastern Cape', [ProvinceController::class, 'easterncape']);
Route::get('/province/kzn', [ProvinceController::class, 'kzn']);
Route::get('/province/Limpopo', [ProvinceController::class, 'limpopo']);
Route::get('/province/Mpumalanga', [ProvinceController::class, 'mpumalanga']);
Route::get('/province/Northern Cape', [ProvinceController::class, 'northerncape']);
Route::get('/province/North West', [ProvinceController::class, 'northwest']);
Route::get('/province/Free state', [ProvinceController::class, 'freestate']);
Route::get('/province/Western Cape', [ProvinceController::class, 'westerncape']);
